var errorCode = context.getVariable("soapResponse.statusCode");
var errorDescription = context.getVariable("soapResponse.statusDesc");
var stateCode = context.getVariable("soapResponse.stateCode");
var state = context.getVariable("hashedState");
var placeCode = context.getVariable("soapResponse.placeCode");
var MCDcode = context.getVariable("soapResponse.MCDcode");
var countyCode = context.getVariable("soapResponse.countyCode");
var placeIncFlag = context.getVariable("soapResponse.placeIncFlag");
var numberOfSpecialTaxDistricts = context.getVariable("soapResponse.numberOfSpecialTaxDistricts");
var deliveryBarCode = context.getVariable("soapResponse.deliveryBarCode");
var spdArray = context.getVariable("soapResponse.spdArray");

//Hash Table to Convert Special Tax District Keys to Jurisdiction Codes
var jurisdictionCodes = {
    "08HSA00019": "08HOA11701",
    "08FSD00005": "08DSD03102",
    "08RTA00001": "08TRD09701",
    "08RTA00002": "08TRD11701",
    "08RTA00003": "08TRD03701",
    "08RTA00008": "08TRD03101",
    "08RTA00011": "08TRD05101",
    "08RTA00014": "08TRD05101",
    "08RTA00015": "08TRD12030",
    "08RTA00017": "08TRD53395",
    "08SCD00004": "08DSC03101",
    "48CCD00047": "48PCC27696",
    "08HSD00020": "08HHS10501"
};


//Build error code object if applicable
if (errorCode == 'Error') {
    var errorObject = {};
    errorObject.code = errorCode.toString();
    errorObject.description = errorDescription.toString();
    //return;
}


//Build Jurisdiction Object if applicable
//create new array
var jurisdictionsObjectArray = [];
// Create new object
var jurisdictionsObject = {};
if (stateCode == 08 || stateCode == 27 || stateCode == 55)
//Build Jurisdiction Type S
{
    jurisdictionsObject.type = 'S';
    jurisdictionsObject.code = stateCode.toString();
} else {
    if (errorCode !== 'Error') {
        var errorObject = {};
        errorObject.code = "NotFound";
        errorObject.description = "State Code not found";
    }
}
jurisdictionsObjectArray.push(jurisdictionsObject);
//Jurisdiction Type C
// Create new object
var jurisdictionsObject = {};
// Default is this; change if needed
jurisdictionsObject.type = 'C';
jurisdictionsObject.code = stateCode.toString() + placeCode.toString();
if (placeCode == '00000' && (stateCode == 55 || stateCode == 27)) {
    if (MCDcode !== "NULL" && MCDcode.length == 5 && (!isNaN(MCDcode))) {
        jurisdictionsObject.type = 'C';
        jurisdictionsObject.code = stateCode.toString() + MCDcode.toString();
    } else {
        jurisdictionsObject.type = 'C';
        jurisdictionsObject.code = stateCode.toString() + placeCode.toString();
    }
} else {
    jurisdictionsObject.type = 'C';
    jurisdictionsObject.code = stateCode.toString() + placeCode.toString();
}
jurisdictionsObjectArray.push(jurisdictionsObject);
//Jurisdiction Type N
// Create new object
var jurisdictionsObject = {};
// Default is this; change if needed
jurisdictionsObject.type = 'N';
jurisdictionsObject.code = stateCode.toString() + countyCode.toString();
if (stateCode == 35) {
    if (placeIncFlag == 'Inc') {
        jurisdictionsObject.type = 'N';
        jurisdictionsObject.code = stateCode.toString() + countyCode.toString() + '-I';
    } else {
        jurisdictionsObject.type = 'N';
        jurisdictionsObject.code = stateCode.toString() + countyCode.toString() + '-O';
    }
}
jurisdictionsObjectArray.push(jurisdictionsObject);
//Jurisdiction Type T or O
var jurisdictionsObject = {};

if (numberOfSpecialTaxDistricts > 0) {
    spdArray.forEach(function(spdArray) {
        if (spdArray.DistrictCode == "TR" || spdArray.DistrictCode == "MTA" || spdArray.DistrictCode == "RTA" || spdArray.DistrictCode == "SUT") {
            jurisdictionsObject.type = 'T';
        } else {
            jurisdictionsObject.type = 'O';
        }
        var specialTaxDistrictKey = stateCode + spdArray.DistrictCode + spdArray.DistrictNumber;
        var specialTaxDistrictKey = specialTaxDistrictKey.toString();
        jurisdictionsObject.code = jurisdictionCodes[specialTaxDistrictKey];
        if (jurisdictionsObject.code === undefined) {
            jurisdictionsObject.code = specialTaxDistrictKey;
        }
        jurisdictionsObjectArray.push({
            type: jurisdictionsObject.type,
            code: jurisdictionsObject.code
        });
    });
}


if (deliveryBarCode == 800143301253) {
    //Jurisdiction Type T or O
    jurisdictionsObjectArray.push({
        type: "T",
        code: '08TRD53395'
    });
    jurisdictionsObjectArray.push({
        type: "T",
        code: '08TRD09701'
    });
    jurisdictionsObjectArray.push({
        type: "T",
        code: '08TRD11701'
    });
    jurisdictionsObjectArray.push({
        type: "O",
        code: '48PCC11701'
    });
    jurisdictionsObjectArray.push({
        type: "O",
        code: '08DSD03102'
    });
    jurisdictionsObjectArray.push({
        type: "O",
        code: '08IMP00007'
    });
    jurisdictionsObjectArray.push({
        type: "O",
        code: '08HHS10501'
    });
    jurisdictionsObjectArray.push({
        type: "O",
        code: '08HOA11701'
    });
}

//Set appropriate object/array for final JSON message
if (errorCode !== 'Error') {
    context.setVariable("jurisdictionsObject", JSON.stringify(jurisdictionsObjectArray));
    context.setVariable("errorObject", JSON.stringify(""));
} else {
    context.setVariable("errorObject", JSON.stringify(errorObject));
    context.setVariable("jurisdictionsObject", JSON.stringify(""));
}


//Get input values for address
var input_line1 = context.getVariable("line1");
var input_line2 = context.getVariable("line2");
var input_line3 = context.getVariable("line3");
var input_line4 = context.getVariable("line4");
var input_city = context.getVariable("city");
var input_state = context.getVariable("state");
var input_country = context.getVariable("country");
var input_postalCode = context.getVariable("postalCode");


var state;
//Convert State from two char to full name and set for final mapping
if (input_state !== "NULL") {
    context.setVariable("jsonResponse.state", input_state);
} else {
    state = "";
    context.setVariable("jsonResponse.state", 'state not found');
}


//Set input values for address to use in final mapping
context.setVariable("jsonResponse.line1", input_line1);
context.setVariable("jsonResponse.line2", input_line2);
context.setVariable("jsonResponse.line3", input_line3);
context.setVariable("jsonResponse.line4", input_line4);
context.setVariable("jsonResponse.city", input_city);
context.setVariable("jsonResponse.state", state);
context.setVariable("jsonResponse.postalCode", input_postalCode);
context.setVariable("jsonResponse.country", input_country);