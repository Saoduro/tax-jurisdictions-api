var jsonResponse = context.getVariable("response.content");
const responseObject = JSON.parse(jsonResponse);

//Set SOAP Response fields to Varibles to be used in mapping later
var statusCode = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["Status.Code"];
var statusDesc = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["Status.Description"];
var stateCode = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["StateCode"];
var state = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["StateProvince"];
var placeCode = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["Place.Code"];
var MCDcode = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["MCD.Code"];
var placeIncFlag = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["Place.IncorporatedFlag"];
var countyCode = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["County.Code"];
var numberOfSpecialTaxDistricts = parseInt(responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["NumberSPDsFound"], 10);
var deliveryBarCode = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["deliveryBarCode"];

//Build Array for Special Tax districts
var spdArray = [];

if (numberOfSpecialTaxDistricts > 0) {
    for (var i = 0; i < numberOfSpecialTaxDistricts; i++) {
        var newSPD = {};
        newSPD.DistrictCode = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["SPD" + (i + 1) + ".DistrictCode"];
        newSPD.DistrictNumber = responseObject.Envelope.Body.AssignGeoTAXInfoResponse.output_port.Address["SPD" + (i + 1) + ".DistrictNumber"];
        spdArray.push(newSPD);
    }

}


//Set Variables for use in MapResponseObjects later
context.setVariable("soapResponse.statusCode", statusCode);
context.setVariable("soapResponse.statusDesc", statusDesc);
context.setVariable("soapResponse.stateCode", stateCode);
context.setVariable("soapResponse.placeCode", placeCode);
context.setVariable("soapResponse.MCDcode", MCDcode);
context.setVariable("soapResponse.placeIncFlag", placeIncFlag);
context.setVariable("soapResponse.countyCode", countyCode);
context.setVariable("soapResponse.numberOfSpecialTaxDistricts", numberOfSpecialTaxDistricts);
context.setVariable("soapResponse.deliveryBarCode", deliveryBarCode);
context.setVariable("soapResponse.state", state);
context.setVariable("soapResponse.spdArray", spdArray);